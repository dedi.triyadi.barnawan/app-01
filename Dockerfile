FROM node:15-alpine
WORKDIR /app

COPY package.json ./
COPY package-lock.json ./
RUN npm install
COPY . ./
RUN npm run build

EXPOSE 5000
ENV HOST=0.0.0.0
CMD ["npm", "start"]
