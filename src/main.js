import App from './App.svelte';

const app = new App({
	target: document.body,
	props: {
		name: 'broo:v4.03'
	}
});

export default app;
